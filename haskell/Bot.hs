module Bot where

import Control.Monad.Trans.State.Lazy
import Debug.Trace
import Control.Monad (join)
import Control.Monad.IO.Class
import Control.Applicative
import Data.Maybe
import Data.List (findIndex,nub,deleteBy)

import GameInitModel
import qualified CarPositionsModel as P
import MetaTypes
import qualified MetaTypes as M (Direction(..))


--this is "main"
botResult :: StateT Status IO Move
botResult = do
    status <- get
    --liftIO $ putStrLn (show $ s_throttle status)
    --logger
    case findMe $ s_positions status of
      Nothing -> error "Can't find you in the position list. Y THIS"
      Just p -> 
        let move = head $ filter (\x->if x==Nothing then False else True) $ 
                                 [ stopDrivingGrandma
                                 , changeLanes
                                 , turboDatShit
                                 , testTrackParams
                                 , fixVelocityBeforeTurn
                                 , goFast
                                 , \_-> Just $ Throttle 1
                                 ] <*> [status] in
          case move of
            Just (Throttle t) -> 
              modify (\x-> x{ s_throttle = t
                            , s_history = (Throttle t):(s_history x)})
              >> (return $ Throttle t)
            Just m -> modify (\x-> x{s_history=m:(s_history x)}) >> return m
            Nothing -> return $ Throttle $ s_throttle status

logger :: StateT Status IO ()
logger = get >>= \status->
    let v = velocity status 
        v0 = velocity $ status{s_posHistory = tail $ s_posHistory status}
        a = v-v0 
        p = P.pieceIndex $ P.piecePosition $ head $ s_posHistory status
        piece = (pieces $ track $ race $ s_init status)!!(P.pieceIndex $ P.piecePosition $ head $ s_posHistory status) in
    liftIO $ appendFile "velocitylog.txt" $ 
    --(show $ pieceLength piece)++"\t"++
    --(show $ radius piece)++"\t"++
    --(show $ pieceAngle piece)++"\t"++
    --(show $ P.inPieceDistance $ P.piecePosition $ head $ s_posHistory status)++"\t"++
    --(show a)++"\t"++
    --(show $ P.angle $ head $ s_posHistory status)++"\t"++
    --(show $ nextV 0.6 v)++"\t"++ --seems close enough accounting for float
    (show p)++"\t"++
    (show $ radius piece)++"\t"++
    (show v)++"\t"++
    (show $ P.angle $ head $ s_posHistory status) ++"\n"


--EXACT if throttle=0.5, seems close enough for other values...
nextV :: Float -> Float -> Float
nextV throttle v = throttle*2-(v/5)

findMe :: [P.CarPosition] -> Maybe P.CarPosition
findMe []     = Nothing
findMe (p:ps) = if (show $ carIdName $ P.carId p) == "\"Team Yak Shave\""
                  then Just p
                  else findMe ps

--AI decision sort-of-predicates--

stopDrivingGrandma :: Status -> Maybe Move
stopDrivingGrandma st = if velocity st < 1
                          then Just $ Throttle 1
                          else Nothing

--make this in StateT and have it do the math...
testTrackParams :: Status -> Maybe Move
testTrackParams st = Nothing

goFast :: Status -> Maybe Move
goFast st = 
    let as = map (abs.P.angle) $ take 2 $ s_posHistory st
        th = s_throttle st
        av = as!!0-as!!1
        pos = P.pieceIndex $ P.piecePosition $ head $ s_posHistory st
        f x= if x>1
               then Just $ Throttle 1
               else if x<0
                      then Just $ Throttle 0
                      else Just $ Throttle x in
      if length as < 2 || 
         (s_justSpawned st)>0 || 
         Nothing/=(pieceLength $ (pieces $ track $ race $ s_init st)!!pos)
        then f 1.0
        else let t | as!!0 < 5  = f $ th
                   | av > 0.6   = f $ th*0.6
                   | av > 0.1   = f $ th*0.99
                   | otherwise  = f 1
                 in t


lessLazyCrashPrevention :: Status -> Float
lessLazyCrashPrevention _ = 0.5

changeLanes :: Status -> Maybe Move
changeLanes st = 
    let p = P.pieceIndex $ P.piecePosition $ head $ s_posHistory st
        ps= (pieces $ track $ race $ s_init st)
        l = P.startLaneIndex $ P.lane $ P.piecePosition $ head $ s_posHistory st
        ls= map distanceFromCenter $ lanes $ track $ race $ s_init st
        nc= (p+1+) $ fromMaybe 0 $ findIndex (\x->Nothing == pieceLength x) $ drop (p+1) ps
        ns= (p+1+) $ fromMaybe 0 $ findIndex (\x->Nothing /= switch x) $ drop (p+1) ps in
      if Nothing/=(s_switching st) || ns>nc || nc+1 > length ps
        then Nothing
        else if (fromMaybe 0 $ pieceAngle (ps!!nc)) < 0
               then if (foldl min 0 ls) == (ls!!l)
                      then Nothing
                      else Just $ LaneChange $ M.Left ns
               else if (foldl max 0 ls) == (ls!!l)
                      then Nothing
                      else Just $ LaneChange $ M.Right ns

fixVelocityBeforeTurn :: Status -> Maybe Move
fixVelocityBeforeTurn st =
    let p = P.pieceIndex $ P.piecePosition $ head $ s_posHistory st
        ps= (pieces $ track $ race $ s_init st) 
        l = P.startLaneIndex $ P.lane $ P.piecePosition $ head $ s_posHistory st
        ls= map distanceFromCenter $ lanes $ track $ race $ s_init st
        p1 = if p+1>= length ps then p+1 - (length ps) else p+1
        p2 = if p+2>= length ps then p+2 - (length ps) else p+2
        tp = head $ filter (\x->if pieceLength x ==Nothing then False else True) $ 
                           drop p ps 
        a = fromMaybe (-1) $ pieceAngle tp
        r = fromMaybe 100 $ radius tp
        ra = (\x-> (fromIntegral r)-((x)*a/(abs a))) $ fromIntegral $ ls!!l in
      if (pieceLength (ps!!(p1)))==Nothing  || (pieceLength (ps!!(p2)))==Nothing
        then fixVelocity (fromMaybe 7 $ join $ (lookup ra $ s_targetVs st)) st
        else Nothing

adjustVForRadius :: Float -> Float -> Float
adjustVForRadius r v = v*(r*r)/10000

fixVelocity :: Float -> Status -> Maybe Move
fixVelocity t st
  | pieceLength p == Nothing = Nothing
  | s_justSpawned st > 0 = f 1
  | v < 0.2 = f 1
  | v-t > 0.5*t = f $ th*0.4
  | v-t > 0.1*t = f $ th*0.6
  | v-t > 0.05*t = f $ th*0.9
  | t-v > 0.5*t = f $ th/0.5
  | t-v > 0.1*t = f $ th/0.8
  | t-v > 0.05*t = f $ th/0.9
  | otherwise = Nothing
  where v = velocity st 
        th = s_throttle st
        f x= if x>1
               then Just $ Throttle 1
               else if x<0
                      then Just $ Throttle 0
                      else Just $ Throttle x
        p = (pieces $ track $ race $ s_init st)!!(P.pieceIndex $ P.piecePosition $ head $ s_posHistory st)

turboDatShit :: Status -> Maybe Move
turboDatShit st = 
    let ls = fromMaybe 100 $ laps $ raceSession $ race $ s_init st
        l = P.lap $ P.piecePosition $ head $ s_posHistory st
        p = P.pieceIndex $ P.piecePosition $ head $ s_posHistory st
        ps= (pieces $ track $ race $ s_init st) in
      if (l==ls-1) && (length $ filter (==Nothing) $ map pieceLength $ drop p ps) ==0
        then trace "FUKKIN TURBO" $ Just Turbo
        else Nothing

combinedSpeedControl :: Status -> Maybe Move
combinedSpeedControl st =
    let a = goFast st
        b = fixVelocityBeforeTurn st in
      case a of
        Nothing -> b
        Just c -> case b of
                    Nothing -> Just c
                    Just d -> Just $ min c d

--end sort-of-predicates--

updatePosition :: Status -> Status
updatePosition s = case findMe (s_positions s) of
                     Nothing -> s --golly I hope this doesn't break things
                     Just m -> s{s_posHistory = m:(s_posHistory s)}

--CURRENT velocity, last 2 ticks
velocity :: Status -> Float
velocity status = 
    if not $ s_started status
      then 0
      else if 2 > (length $ s_posHistory status)
             then 0
             else (\x-> (totalDistanceFull status $ head x) - (totalDistanceFull status $ last x)) $ take 2 $ map P.piecePosition $ s_posHistory status

getLaneLength :: Status -> Int -> Piece -> Float
getLaneLength s l p =
    case pieceLength p of
      Just l -> l
      Nothing -> lengthOfArc s l p

lengthOfArc :: Status -> Int -> Piece -> Float
lengthOfArc st l p = 
    let ll= fromIntegral $ distanceFromCenter $ (lanes $ track $ race $ s_init st) !! l
        a = pieceAngle p 
        r = (\x y-> (fromIntegral x)-(ll*y/(abs y))) <$> radius p <*> a in
      case (*) <$> ((2*pi*) <$> r) <*> ((/360.0) <$> a) of
        Nothing -> error $ "lengthOfArc is still fukt"
        Just f -> f

--calculates it without knowledge of last position's total distance.
--could be done way more efficiently... #yolo
totalDistanceFull :: Status -> P.PiecePosition -> Float
totalDistanceFull status pp =
  let lane= P.startLaneIndex $ P.lane pp
      pcs = map (getLaneLength status lane) $ pieces $ track $ race $ s_init status
      ipd = P.inPieceDistance pp
      pcs2 = take (1 + P.pieceIndex pp) pcs
      lap = fromIntegral $ P.lap pp in
    (lap * sum pcs) + (sum pcs2) + ipd

updateMaxAngle :: StateT Status IO ()
updateMaxAngle = get >>= \st->
    let a = P.angle $ head $ s_posHistory st in
      if abs a > s_maxAngle st
        then modify (\x->x{s_maxAngle=a})
        else return ()

assignInitialVs :: StateT Status IO ()
assignInitialVs = get >>= \st->
    let ps = map radius $ pieces $ track $ race $ s_init st
        ls = map distanceFromCenter $ lanes $ track $ race $ s_init st
        ts = map fromIntegral $ map (fromMaybe 0) $ filter (/=Nothing) $ nub $ fmap <$> ([(+),(-)] <*> ls) <*> ps in
      modify (\x-> x{s_targetVs= map (\x-> (x,_assignInitialV x)) ts})

_assignInitialV :: Float -> Maybe Float
_assignInitialV ra = Just $ adjustVForRadius ra 6


updateTargetV :: Float -> Float -> Bool -> Float
updateTargetV tv angle crash = 
    if crash
      then
        tv*0.4
      else let a | abs angle >40 = tv
                 | abs angle >30 = tv*1.05
                 | abs angle >15 = tv*1.1
                 | abs angle >5  = tv*1.2
                 | abs angle <1  = tv*1.5
                 | otherwise     = tv*1.3 in a
  
--assuming the finish line isn't on a curve
updateTargetVs :: Int -> StateT Status IO ()
updateTargetVs poop = get >>= \st->
    let pl = pieces $ track $ race $ s_init st
        relevant = map pieceLength $ pl 
        p = if poop>=(length pl) then poop-(length pl) else poop 
        p1 = if poop+1>=(length pl) then poop+1-(length pl) else poop+1 in
      case relevant!!p of
        Nothing -> 
          updateTargetVs $ (+1) $ ((length pl)-(length $ dropWhile ((/=Nothing).pieceLength) $ drop (p+1) pl))
        Just _ -> do
          st <- get
          let angle = s_maxAngle st
              tv = fromMaybe 0 $ join $ lookup angle $ s_targetVs st
              ra = P.angle $ head $ drop 1 $ s_posHistory st
              pv = join $ lookup ra $ s_targetVs st in
            (return $ updateTargetV tv (s_maxAngle st) True)
            >>= (\y-> modify (\x->x{s_targetVs= 
                                    (ra,Just $ updateTargetV tv y True):
                                      (deleteBy (\(i,_) (j,_)-> i==j) 
                                               (ra,Just y) $ 
                                               s_targetVs st)})) >>
              if length pl < 2
                then return ()
                else if trace (show p) $ (pieceLength $ pl!!(p1))==Nothing
                       then return ()
                       else updateTargetVs (p+1)



