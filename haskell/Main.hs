{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Monad.Trans.State.Lazy
import Control.Monad.IO.Class
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Debug.Trace

import GameInitModel
import qualified CarPositionsModel as P
import MetaTypes
import qualified MetaTypes as M (Direction(..))

import Bot

type ClientMessage = String

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"
laneChangeMessage d = "{\"msgType\":\"switchLane\",\"data\":\""++(d)++"\"}"
turboMessage = "{\"msgType\": \"turbo\", \"data\": \"<insert turbo pun>\"}"
customMessage track botname botkey = "{\"msgType\":\"createRace\",\"data\":{\"botId\":{\"name\":\""++botname++"\",\"key\":\""++botkey++"\"},\"trackName\":\""++track++"\",\"password\":\"dickbutt\",\"carCount\":1}}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey =  do
      h <- connectToServer server port
      hSetBuffering h LineBuffering
      hPutStrLn h $ joinMessage botname botkey
      --hPutStrLn h $ customMessage "germany" botname botkey
      return h
      runStateT (handleMessages h) blankStatus

handleMessages :: Handle -> StateT Status IO ()
handleMessages h = do
  msg <- liftIO $ hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = Join | YourCar CarId | GameInit GameInitData | CarPositions [P.CarPosition] | Unknown String | Crash CarId | Start | Spawn | Lap

handleServerMessage :: Handle -> ServerMessage -> StateT Status IO ()
handleServerMessage h serverMessage = do
  responses <- respond serverMessage
  forM_ responses $ (liftIO.(hPutStrLn h))
  handleMessages h

--yes, I know this is called respond. That's what HWO called it. But
--I turned it into an event handler. Deal with it.
respond :: ServerMessage -> StateT Status IO [ClientMessage]
respond message = case message of
  Join -> do
    liftIO $ putStrLn "Joined"
    return [pingMessage]
  YourCar car -> do
    modify (\x-> x{s_car=car})
    trace (show car) $ return [pingMessage]
  GameInit gameInit -> do
    liftIO $ putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    modify (\x-> x{s_init=gameInit,s_started=True})
    assignInitialVs
    return [pingMessage]
  CarPositions carPositions -> do
    --general bookkeeping
    modify (\x-> updatePosition $ x{s_positions=carPositions,s_justSpawned = (s_justSpawned x)-1})
    updateMaxAngle
    st <- get
    --update target velocity for last piece
    if (length $ s_posHistory st) >2
      then
        let pi = P.pieceIndex $ P.piecePosition $ head $ drop 1 $ s_posHistory st
            p  = P.pieceIndex $ P.piecePosition $ head $ s_posHistory st
            pl = P.startLaneIndex $ P.lane $ P.piecePosition $ head $ drop 1 $ s_posHistory st
            ra = P.angle $ head $ drop 1 $ s_posHistory st
            pv = join $ lookup ra $ s_targetVs st in
          if pi /= p
            then 
              updateTargetVs p
              >> modify (\x->x{s_maxAngle=0,s_didWeCrash=False})
            else return () 
      else return ()
    --check if we've switched lanes
    case s_switching st of
      Nothing -> return ()
      Just l -> if (P.pieceIndex $ P.piecePosition $ head $ s_posHistory st) >= l
                  then modify (\x->x{s_switching = Nothing})
                  else return ()
    --run the bot
    br <- botResult
    modify (\x-> x{s_next = br, s_history = br:(s_history x)})
    case br of 
      Throttle t -> return [throttleMessage t]
      LaneChange (M.Left l) -> do
        modify (\x-> x{s_switching = Just l})
        return [laneChangeMessage "Left"]
      LaneChange (M.Right l) -> do
        modify (\x-> x{s_switching = Just l})
        return [laneChangeMessage "Right"]
      Turbo -> return [turboMessage]
  Crash id -> do
    modify (\x->x{s_didWeCrash=True})
    modify (\x->x{s_switching = Nothing})
    trace ("CRASH:" ++ show id) $ return [pingMessage]
  Start -> do
    trace "START! (sort of)" $ return [pingMessage]
  Spawn -> do
    modify (\x->x{s_justSpawned = 2})
    trace "Spawn" $ return [throttleMessage 1]
  Lap -> do
    trace "Lap" $ return [pingMessage]
  Unknown msgType -> do
    liftIO $ putStrLn $ "Unknown message: " ++ msgType
    return [pingMessage]

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join" = Success Join
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | msgType == "yourCar" = YourCar <$> (fromJSON msgData)
  | msgType == "crash" = Crash <$> (fromJSON msgData)
  | msgType == "gameStart" = Success Start 
  | msgType == "error" = error $ show msgData
  | msgType == "spawn" = Success Spawn
  | msgType == "lapFinished" = Success Lap
  | otherwise = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
