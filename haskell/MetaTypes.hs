--this is an attempt to make the very verbose types given in the example
--bot more useful for my algorithmic use. I think I should be using lenses
--for this instead, but I don't know how to use them :/

module MetaTypes where

import Data.Functor

import GameInitModel
import qualified CarPositionsModel as P


data Direction = Left Int | Right Int deriving (Show,Eq,Ord)
data Move = Throttle Float | LaneChange Direction | Turbo deriving (Show,Eq,Ord)

data Status = Status { s_init :: GameInitData
                     , s_car :: CarId
                     , s_next :: Move
                     , s_positions :: [P.CarPosition]
                     , s_justSpawned :: Int --number of updates before care about angle
                     , s_throttle :: Float
                     , s_history :: [Move] --necessary?
                     , s_posHistory :: [P.CarPosition] --head is most recent
                     , s_started :: Bool
                     , s_targetVs :: [(Float,Maybe Float)] --(radius,target)
                     , s_didWeCrash :: Bool
                     , s_maxAngle :: Float
                     , s_switching :: Maybe Int
                     } deriving (Show)

blankId = CarId "fake" "fake"

blankPiecePosition = P.PiecePosition 0 0 (P.CarLane 0 0) 0

--god this is ugly, keep it folded
blankStatus :: Status
blankStatus = Status { s_init = 
                         GameInitData { race = Race {track = Track {name = "Fake", startingPoint = StartingPoint {position = Position {x = -300.0, y = -44.0}, angle = 90.0}
                                                                   , pieces = []
                                                                   , lanes = []
                                                                   }
                                      , cars = [Car {carId = CarId {color = "red", carIdName = "Team Yak Shave"}, dimensions = Dimension {dimensionLength = 40.0, width = 20.0, guideFlagPosition = 10.0}}]
                                      , raceSession = RaceSession {laps = Just 3, durationMs = Nothing, maxLapTimeMs = Just 60000, quickRace = Just True}}}
                     , s_car = blankId
                     , s_positions = [P.CarPosition{ P.carId = blankId
                                                  , P.angle = 0
                                                  , P.piecePosition = blankPiecePosition
                                                  }]
                     , s_justSpawned = 0
                     , s_history = []
                     , s_posHistory = []
                     , s_started = False
                     , s_throttle = 0
                     , s_next = Throttle 0.5
                     , s_targetVs = []
                     , s_didWeCrash = False
                     , s_maxAngle = 0
                     , s_switching = Nothing
                     }

